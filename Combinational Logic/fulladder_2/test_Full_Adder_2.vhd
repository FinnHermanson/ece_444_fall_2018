library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

use std.textio.all ;
use ieee.std_logic_textio.all;

entity test_Full_Adder_2 is
end;

architecture test of test_Full_Adder_2 is 

component Full_Adder_2
	port (
		S				: out std_logic;
		C_out			: out std_logic;
		x				: in std_logic;
		y				: in std_logic;
		C_in			: in std_logic);
end component;

signal input			: std_logic_vector(2 downto 0);
signal test_output		: std_logic_vector(1 downto 0);
signal exp_output		: std_logic_vector(1 downto 0);

begin
	dev_to_test: Full_Adder_2 port map (
		S => test_output(0), C_out => test_output(1),
		x => input(0), y => input(1), C_in => input(2));
		
	-- create expected output behavior
	expected_proc : process(input)
		begin
			case input is
			when "000" => exp_output <= "00";
			when "001" | "010" | "100" => exp_output <= "01";
 			when "011" | "110" | "101" => exp_output <= "10";
			when "111" => exp_output <= "11";
			when others => exp_output <= (others => 'X');
			end case;
		end process expected_proc;
		
		stimulus : process
		-- Variables for testbench
		variable ErrCnt : integer := 0;
		variable WriteBuf: line ;
		
		begin
			for i in 0 to 7 loop
				input <= std_logic_vector(to_unsigned(i,input'length));
				
				wait for 10 ns;
				if(exp_output /= test_output) then	
					write(WriteBuf, string'("ERROR FA test failed at: input = "));
					write(WriteBuf, input);
					writeline(Output, WriteBuf);
					
					write(WriteBuf, string'("Output = ")); write(WriteBuf, test_output);
					write(WriteBuf, string'(". Exp. Output = ")); write(WriteBuf, exp_output);
					
					writeline(Output, WriteBuf);
					
					ErrCnt := ErrCnt+1;
				end if;
			end loop; -- ieee
			
			if (ErrCnt = 0) then
				report "SUCCESS! Full Adder Test Completed.";
			else
				report "The Full Adder is broken." severity error;
			end if;
		end process stimulus;
	end test;
	