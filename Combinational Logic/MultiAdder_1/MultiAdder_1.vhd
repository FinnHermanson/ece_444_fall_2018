library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity MultiAdder_1 is
	generic(bit_depth		:	integer :=8);
	Port (
		result				: out signed(bit_depth-1 downto 0);
		A, B, C, D, E, F	: in signed(bit_depth-1 downto 0)
	);
end MultiAdder_1;

architecture behavior of MultiAdder_1 is
begin

	result <= (A+B) + (C+D) + (E+F);
	
end behavior;