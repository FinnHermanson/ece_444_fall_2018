library IEEE;
use IEEE.std_logic_1164.all;

entity decoder_2 is
Port (
	decode				: out std_logic_vector(7 downto 0);
	code				: in std_logic_vector(2 downto 0)
);
end decoder_2;

architecture behavior of decoder_2 is

begin

	decode_proc: process(code)
	begin
		case code is
			when "000" =>
				decode <= "00000001";
			when "001" =>
				decode <= "00000010";
			when "010" =>
				decode <= "00000100";
			when "011" =>
				decode <= "00001000";
			when "100" =>
				decode <= "00010000";
			when "101" =>
				decode <= "00100000";
			when "110" =>
				decode <= "01000000";
			when "111" =>
				decode <= "10000000";
			when others =>
				decode <= "XXXXXXXX";
		end case;
	end process;
end behavior;