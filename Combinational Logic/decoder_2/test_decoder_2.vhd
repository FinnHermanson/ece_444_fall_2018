library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

use std.textio.all;
use ieee.std_logic_textio.all ;

entity test_decoder_2 is
end;

architecture test of test_decoder_2 is

component decoder_2
	Port (
		decode				: out std_logic_vector(7 downto 0);
		code				: in std_logic_vector(2 downto 0)
	);
end component;

signal code_in				: std_logic_vector(2 downto 0);
signal decode_out			: std_logic_vector(7 downto 0);
signal expected				: std_logic_vector(7 downto 0);

begin

dev_to_test : decoder_2 port map (
	decode => decode_out,
	code => code_in
	);
	
expected_proc : process(code_in)
	begin
	case code_in is
		when "000" =>
			expected <= "00000001";
		when "001" =>
			expected <= "00000010";
		when "010" =>
			expected <= "00000100";
		when "011" =>
			expected <= "00001000";
		when "100" =>
			expected <= "00010000";
		when "101" =>
			expected <= "00100000";
		when "110" =>
			expected <= "01000000";
		when "111" =>
			expected <= "10000000";
		when others =>
			expected <= "XXXXXXXX";
	end case;
end process expected_proc;

stimulus : process

	variable ErrCnt : integer := 0 ;
	variable WriteBuf : line ;

	begin
	for i in 0 to 7 loop
		code_in <= std_logic_vector(to_unsigned(i,code_in'length));
	
		wait for 10 ns;
	
		if(decode_out /= expected) then
			write(WriteBuf, string'("ERROR: decoder test failed at code "));
			write(WriteBuf, code_in);
			writeline(Output, WriteBuf);
			ErrCnt := ErrCnt+1;
		end if;
	end loop;

	if (ErrCnt = 0) then
		report "SUCCESS. Decoder Completed";
	else
		report "The decoder is broken" severity warning;
	end if;


end process stimulus;
end test;