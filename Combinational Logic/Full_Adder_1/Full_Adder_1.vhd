-- Dataflow Full Adder
library IEEE;
use IEEE.STD_LOGIC_1164.All;

entity Full_Adder_1 is
	port (
		S						: out std_logic;
		C_out					: out std_logic;
		x						: in std_logic;
		y						: in std_logic;
		C_in					: in std_logic);
end Full_Adder_1;

architecture dataflow of Full_Adder_1 is 

begin
	C_out <= (C_in and (x xor y)) or (x and y);
	S <= x xor y xor C_in;
end dataflow;