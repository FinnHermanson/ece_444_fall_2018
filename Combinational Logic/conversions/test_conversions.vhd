library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

use std.textio.all ;
use ieee.std_logic_textio.all ;

entity test_conversions is
end;

architecture test of test_conversions is
  
component conversions
generic (
	input_size : integer := 4
	);
port (
	output_sv:		out signed(input_size-1 downto 0);
	output_uv:		out unsigned(input_size-1 downto 0);
	output_int:		out integer range 0 to 2**input_size-1;		
    output_slv:		out std_logic_vector(input_size-1 downto 0);

	input_sv:		in signed(input_size-1 downto 0);
	input_uv:		in unsigned(input_size-1 downto 0);
	input_int:		in integer range 0 to 2**input_size-1;		
    input_slv:		in std_logic_vector(input_size-1 downto 0);
	
	opcode:			in std_logic_vector(1 downto 0));
end component;

constant input_size : integer := 4;

signal input_sv : 	signed(input_size-1 downto 0);
signal input_uv : 	unsigned(input_size-1 downto 0);
signal input_int : 	integer range 0 to 2**input_size-1;
signal input_slv :	std_logic_vector(input_size-1 downto 0);
 
signal output_sv : 	signed(input_size-1 downto 0);
signal output_uv : 	unsigned(input_size-1 downto 0);
signal output_int : 	integer range 0 to 2**input_size-1;
signal output_slv :	std_logic_vector(input_size-1 downto 0);

signal opcode : std_logic_vector(1 downto 0);

begin
  
dev_to_test:  conversions 
generic map (input_size)
port map(output_sv, output_uv, output_int, output_slv,
	input_sv, input_uv, input_int, input_slv, opcode);
  
stimulus : process

  -- Variables for testbench
  variable ErrCnt : integer := 0 ;
  variable WriteBuf : line ;
  
  begin
    for i in 0 to 2**(input_size-1)-1 loop
		input_sv <= to_signed(i,input_size);
		input_uv <= to_unsigned(i,input_size);
		input_int <= i;
		input_slv <= std_logic_vector(to_unsigned(i,input_size));
		
		for j in 0 to 3 loop
			opcode <= std_logic_vector(to_unsigned(j,2));
			
			wait for 10 ns;
			
		end loop;
      
      
  end loop;
    
end process stimulus;

end test;