transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\conversions}
vlib work
vmap work work

# compile the program and test-bench files
vcom conversions.vhd
vcom test_conversions.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_conversions
add wave sim:/test_conversions/dev_to_test/*
 
# define simulation time
run 650 ns
# zoom out
wave zoom full