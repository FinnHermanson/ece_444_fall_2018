transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {D:\Documents\School\Fourth_Year\ECE_444_501\VHDL\Combinational_Logic\operators}
vlib work
vmap work work

# compile the program and test-bench files
vcom operators.vhd
vcom test_operators.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_operators
add wave sim:/test_operators/dev_to_test/*
 
# define simulation time
run 11 us
# zoom out
wave zoom full
