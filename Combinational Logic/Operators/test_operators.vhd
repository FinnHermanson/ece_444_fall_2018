-- testbench for operators.vhd
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
USE ieee.math_real.ALL;   -- for UNIFORM, TRUNC functions

entity test_operators is
end;

architecture test of test_operators is

component operators
generic (
	bit_depth 				: integer := 32);
port (
	output					: out signed(bit_depth-1 downto 0);
	input_1					: in signed(bit_depth-1 downto 0);
	input_2					: in signed(bit_depth-1 downto 0);
	opcode					: in std_logic_vector(7 downto 0));
end component;

constant bit_depth			: integer := 32;
constant max_input_value 	: integer := 2**(bit_depth-2);
constant mid_point			: integer := max_input_value / 2;

signal in1					: signed(bit_depth-1 downto 0) := (others => '0');
signal in2					: signed(bit_depth-1 downto 0) := (others => '0');
signal outsig      			: signed(bit_depth-1 downto 0);
signal code 				: std_logic_vector(7 downto 0) := (others => '0');

begin

	dev_to_test : operators 
		generic map(bit_depth => bit_depth)
		port map (
			output => outsig,
			input_1 => in1,
			input_2 => in2,
			opcode => code);
	
	stimulus : process	
	-- Variables for testbench
	variable ErrCnt 			: integer := 0 ;
	variable seed1, seed2		: positive; -- Seed values for random generator
	variable rand, rval			: real; -- Random real-number value in range 0 to 1.0

	begin
		
		for i in 0 to 10 loop
			code <= std_logic_vector(to_unsigned(i,8));
			for j in 0 to 99 loop
		  
				UNIFORM(seed1, seed2, rand); -- generate random number
				rval := trunc(rand*real(max_input_value));
				in1 <= to_signed(integer(rval)-mid_point,bit_depth);
				
				UNIFORM(seed1, seed2, rand); -- generate random number
				rval := trunc(rand*real(max_input_value));
				in2 <= to_signed(integer(rval)-mid_point,bit_depth);
			
				wait for 10 ns;
								
			end loop;            
		end loop;      

		report "Operators Test Completed.";
	end process stimulus;	
end test;
	 

