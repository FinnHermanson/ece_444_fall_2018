library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity operators is
generic (
	bit_depth			: integer :=32);
port (
	output				: out signed(bit_depth-1 downto 0);
	input_1				: in signed(bit_depth-1 downto 0);
	input_2				: in signed(bit_depth-1 downto 0);
	opcode				: in std_logic_vector(7 downto 0));
end operators;

architecture behavior of operators is

		signal opcode_int		: integer := 0;
		constant half_size		: integer := bit_depth/2;
		
		begin
		
		opcode_int <= to_integer(unsigned(opcode));
		
		op: process(input_1, input_2, opcode_int)
			begin
			case opcode_int is
				when 0 =>
					output <= not input_1;
				when 1 =>
					output <= input_1 mod input_2;
				when 2 =>
					output <= input_1 rem input_2;
				when 3 =>
					output <= input_1 + input_2;
				when 4 =>
					output <= input_1 - input_2;
				when 5 =>
					output <= input_1(bit_depth-1 downto half_size) & input_2(half_size-1 downto 0);
				when 6 =>
					output <= abs(input_1);
				when 7 =>
					output <= not(input_2);
				when 8 =>
					output <= input_1(half_size-1 downto 0) * input_2(half_size-1 downto 0);
				when 9 =>
					output <= input_1 xor input_2;
				when 10 =>
					output <= input_1 and input_2;
				when others =>
					output <= (others => 'X');
			end case;
		end process op;
end behavior;
	