transcript off
quit -sim

cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\priority_encoder_1}

vlib work
vmap work work

vcom priority_encoder_1.vhd
vcom test_priority_encoder_1.vhd

vsim test_priority_encoder_1
add wave sim:/test_priority_encoder_1/dev_to_test/*

run 80 ns
wave zoom full