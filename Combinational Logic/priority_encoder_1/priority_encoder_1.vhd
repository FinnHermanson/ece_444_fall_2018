library IEEE;
use IEEE.std_logic_1164.all;

entity priority_encoder_1 is
Port (
	encode					: out std_logic_vector(2 downto	0);
	a_in					: in std_logic_vector(7 downto 0)
);
end priority_encoder_1;

architecture dataflow of priority_encoder_1 is

begin

	encode(2) <= ('0' and a_in(0)) or
				('0' and a_in(1) and not a_in(0)) or
				('0' and a_in(2) and not a_in(1) and not a_in(0)) or
				('0' and a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('1' and a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('1' and a_in(5) and not a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('1' and a_in(6) and not a_in(5) and not a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('1' and a_in(7) and not a_in(6) and not a_in(5) and not a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0));
	encode(1) <= ('0' and a_in(0)) or
				('0' and a_in(1) and not a_in(0)) or
				('1' and a_in(2) and not a_in(1) and not a_in(0)) or
				('1' and a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('0' and a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('0' and a_in(5) and not a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('1' and a_in(6) and not a_in(5) and not a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('1' and a_in(7) and not a_in(6) and not a_in(5) and not a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0));
	encode(0) <= ('0' and a_in(0)) or
				('1' and a_in(1) and not a_in(0)) or
				('0' and a_in(2) and not a_in(1) and not a_in(0)) or
				('1' and a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('0' and a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('1' and a_in(5) and not a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('0' and a_in(6) and not a_in(5) and not a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0)) or
				('1' and a_in(7) and not a_in(6) and not a_in(5) and not a_in(4) and not a_in(3) and not a_in(2) and not a_in(1) and not a_in(0));
end dataflow;