library IEEE;
use IEEE.STD_LOGIC_1164.All;
use IEEE.numeric_std.all;
use ieee.math_real.all;

entity test_adder_1 is
end;

architecture test of test_adder_1 is

component adder_1
	generic(bit_depth				: integer := 32);
	Port (
		s							: out std_logic_vector(bit_depth-1 downto 0);
		c_out						: out std_logic;
		x							: in std_logic_vector(bit_depth-1 downto 0);
		y							: in std_logic_vector(bit_depth-1 downto 0);
		c_in						: in std_logic
	);
end component;

constant bit_depth					: integer := 32;
constant max_input_value			: integer := 2**(bit_depth-2);
signal x							: unsigned(bit_depth-1 downto 0) := (others => '0');
signal y							: unsigned(bit_depth-1 downto 0) := (others => '0');
signal c_in							: std_logic := '0';
signal s							: unsigned(bit_depth-1 downto 0) := (others => '0');
signal c_out						: std_logic := '0';

begin

	dev_to_test : adder_1
		generic map(
			bit_depth => bit_depth)
		port map (
			unsigned(s) => s,
			c_out => c_out,
			x => std_logic_vector(x),
			y => std_logic_vector(y),
			c_in => c_in);
	
	stimulus : process
	-- Variables for testbench
	variable ErrCnt					: integer := 0;
	variable seed1, seed2			: positive;
	variable rand, rval				: real;
	
	begin
		for i in 0 to 99999 loop
		
			UNIFORM(seed1, seed2, rand);
			rval := trunc(rand*real(max_input_value));
			x <= to_unsigned(integer(rval),bit_depth);
			
			UNIFORM(seed1, seed2, rand);
			rval := trunc(rand*real(max_input_value));
			y <= to_unsigned(integer(rval),bit_depth);
			
			wait for 10 ns;
			
			if ((c_out & s) /= x+y) then
				report "The adder is broken" severity error;
				ErrCnt := ErrCnt+1;
			end if;
		end loop;
		
		if (ErrCnt = 0) then
			report "Success! Adder test completed.";
		else
			report "The adder is broken." severity error;
		end if;
	end process stimulus;
end test;