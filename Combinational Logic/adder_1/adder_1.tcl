transcript off
#stop previous simulations
quit -sim

# select a directory for the creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\adder_1}
vlib work
vmap work work 

# compile the program and test_bench files
vcom ../full_adder_1/full_adder_1.vhd
vcom adder_1.vhd
vcom test_adder_1.vhd

# initialize the simulation window and add waves to the simulation window
vsim test_adder_1
add wave sim:/test_adder_1/dev_to_test/*

# define simulation time
run 1 ns
#zoom out
wave zoom full