library IEEE;
use IEEE.STD_LOGIC_1164.All;

entity adder_1 is
	generic(
		bit_depth			: integer := 32);
	Port (
		s					: out std_logic_vector(bit_depth-1 downto 0);
		c_out				: out std_logic;
		x					: in std_logic_vector(bit_depth-1 downto 0);
		y					: in std_logic_vector(bit_depth-1 downto 0);
		c_in				: in std_logic
	);
end adder_1;

architecture structure of adder_1 is

component full_adder_1 is
	Port (
		s					: out std_logic;
		c_out				: out std_logic;
		x					: in std_logic;
		y					: in std_logic;
		c_in				: in std_logic
	);
end component;

signal carry				:std_logic_vector(bit_depth downto 0);
begin
	carry(0) <= c_in;
	c_out <= carry(bit_depth-1);
	
	full_adders:
	for i in 0 to bit_depth-1 generate
		sum_x : full_adder_1 port map(s(i), carry(i+1), x(i), y(i), carry(i));
	end generate full_adders;
	
end structure;