library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decoder_1 is
Port(
	decode 			: out std_logic_vector(7 downto 0);
	code			: in std_logic_vector(2 downto 0)
);
end decoder_1;

architecture dataflow of decoder_1 is
begin

	decode(0) <= not code(2) and not code(1) and not code(0);
	decode(1) <= not code(2) and not code(1) and code(0);
	decode(2) <= not code(2) and code(1) and not code(0);
	decode(3) <= not code(2) and code(1) and code(0);
	decode(4) <= code(2) and not code(1) and not code(0);
	decode(5) <= code(2) and not code(1) and not code(0);
	decode(6) <= code(2) and code(1) and not code(0);
	decode(7) <= code(2) and code(1) and code(0);
	
end dataflow;