library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use ieee.math_real.all;   -- for UNIFORM, TRUNC functions

use std.textio.all ;
use ieee.std_logic_textio.all ;

entity test_NestedIFS_1 is
end;

architecture test of test_NestedIFS_1 is

component NestedIFS_1
	generic(bit_depth				: integer := 8);
	Port (
		result						: out signed(bit_depth-1 downto 0);
		A							: in signed(bit_depth-1 downto 0);
		B							: in signed(bit_depth-1 downto 0);
		enable						: in std_logic;
		actv_low					: in std_logic;
		opcode						: in std_logic_vector(2 downto 0)
	);
end component;

constant bit_depth					: integer := 8;
constant max_input_value 			: integer := 2**(bit_depth)-1;
constant mid_point					: integer := max_input_value/2;

signal result_out					: signed(bit_depth-1 downto 0);
signal A_in							: signed(bit_depth-1 downto 0) := (others => '0');
signal B_in							: signed(bit_depth-1 downto 0) := (others => '0');
signal enable_in					: std_logic := '1';
signal actv_low_in					: std_logic := '1';
signal opcode_in					: unsigned(2 downto 0) := (others => '0');
signal expected_high				: signed(bit_depth-1 downto 0);
signal expected_res					: signed(bit_depth-1 downto 0);

begin

	dev_to_test : NestedIFS_1 
	generic map(bit_depth)
	port map (
		result => result_out,
		A => A_in,
		B => B_in,
		enable => enable_in,
		actv_low => actv_low_in,
		opcode => std_logic_vector(opcode_in));
		
	output_proc : process(expected_high, enable_in, actv_low_in)
	begin
		if(enable_in = '0') then
		  expected_res <= (others => '0');
		elsif(actv_low_in = '1') then
		  expected_res <= not expected_high;
		else
		  expected_res <= expected_high;
		end if;
	end process output_proc;

	opcode_proc : process(opcode_in, A_in, B_in)
	begin
		case opcode_in is
		  when "000" =>
			expected_high <= A_in;
		  when "001" =>
			expected_high <= B_in;
		  when "010" =>
			expected_high <= not A_in;
		  when "011" =>
			expected_high <= not B_in;
		  when "100" =>
			expected_high <= A_in + B_in;
		  when "101" =>
			expected_high <= A_in - B_in;
		  when others =>
			expected_high <= (others => '0');
		  end case;        
	end process opcode_proc;

	stimulus : process	
	-- Variables for testbench
	variable ErrCnt : integer := 0 ;
	variable WriteBuf : line ;
	variable seed1, seed2		: positive; -- Seed values for random generator
	variable rand, rval			: real; -- Random real-number value in range 0 to 1.0
	  
	begin
		
		for j in std_logic range '0' to '1' loop
			enable_in <= j;
			for k in std_logic range '0' to '1' loop
				actv_low_in <= k;
				for m in 0 to 5 loop
					opcode_in <= to_unsigned(m,opcode_in'length);
					for i in 0 to 9999 loop		
						uniform(seed1, seed2, rand);
						rval := trunc(rand*real(max_input_value));
						A_in <= to_signed(integer(trunc(rval))-mid_point,bit_depth);

						uniform(seed1, seed2, rand);
						rval := trunc(rand*real(max_input_value));
						B_in <= to_signed(integer(trunc(rval))-mid_point,bit_depth);
												   
						wait for 10 ns;
							
						if (expected_res /= result_out) then
							write(WriteBuf, string'("ERROR  ALU test failed at: Enable = "));
							write(WriteBuf, enable_in); 
							write(WriteBuf, string'(", A_in = "));
							write(WriteBuf, to_integer(unsigned(A_in)));
							write(WriteBuf, string'(", B_in = "));
							write(WriteBuf, to_integer(unsigned(B_in)));
							write(WriteBuf, string'(", actv_low = "));
							write(WriteBuf, actv_low_in);
							write(WriteBuf, string'(", opcode = "));
							write(WriteBuf, to_integer(opcode_in));
						  
							writeline(Output, WriteBuf);
							ErrCnt := ErrCnt+1;
						end if;		 
					end loop; -- i
				end loop; -- m
			end loop; -- k
		end loop; -- j		  

		if (ErrCnt = 0) then 
			report "SUCCESS!!!  ALU Test Completed";
		else
			report "The ALU is broken" severity warning;
		end if;
	end process stimulus;
end test;