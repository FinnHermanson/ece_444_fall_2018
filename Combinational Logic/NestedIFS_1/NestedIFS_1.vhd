library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity NestedIFS_1 is
	generic(bit_depth			: integer := 32);
	Port (
		result					: out signed(bit_depth-1 downto 0);
		A						: in signed(bit_depth-1 downto 0);
		B						: in signed(bit_depth-1 downto 0);
		enable					: in std_logic;
		actv_low				: in std_logic;
		opcode					: in std_logic_vector(2 downto 0)
	);
end NestedIFS_1;

architecture behavior of NestedIFS_1 is
begin
	result_proc: process(A, B, enable, actv_low, opcode)
	begin
		if(actv_low = '0') then
			case opcode is
				when "000" =>
					if(enable = '1') then
						result <= A;
					else
						result <= (others => '0');
					end if;
				when "001" =>
					if(enable = '1') then
						result <= B;
					else
						result <= (others => '0');
					end if;
				when "010" =>
					if(enable = '1') then
						result <= not A;
					else
						result <= (others => '0');
					end if;
				when "011" =>
					if(enable = '1') then
						result <= not B;
					else
						result <= (others => '0');
					end if;
				when "100" =>
					if(enable = '1') then
						result <= A+B;
					else
						result <= (others => '0');
					end if;
				when "101" =>
					if(enable = '1') then
						result <= A-B;
					else
						result <= (others => '0');
					end if;
				when others =>
						result <= (others => 'X');
			end case;
		else
			case opcode is
				when "000" =>
					if(enable = '1') then
						result <= not A;
					else
						result <= (others => '0');
					end if;
				when "001" =>
					if(enable = '1') then
						result <= not B;
					else
						result <= (others => '0');
					end if;
				when "010" =>
					if(enable = '1') then
						result <= A;
					else
						result <= (others => '0');
					end if;
				when "011" =>
					if(enable = '1') then
						result <= B;
					else
						result <= (others => '0');
					end if;
				when "100" =>
					if(enable = '1') then
						result <= not (A+B);
					else
						result <= (others => '0');
					end if;
				when "101" =>
					if(enable = '1') then
						result <= not (A-B);
					else
						result <= (others => '0');
					end if;
				when others =>
					result <= (others => 'X');
			end case;
		end if;
	end process;
end behavior;
					
					