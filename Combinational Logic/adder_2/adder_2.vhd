library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity adder_2 is
	generic(
		bit_depth			: integer := 32);
	Port (
		x					: in unsigned(bit_depth-1 downto 0);
		y					: in unsigned(bit_depth-1 downto 0);
		c_in				: in std_logic;
		s					: out unsigned(bit_depth-1 downto 0);
		c_out				: out std_logic
	);
end adder_2;

architecture behavior of adder_2 is

	signal sum				: unsigned(bit_depth+1 downto 0);
	
	begin
		sum <= ("0" & x & "1") + ("0" & y & c_in);
		
		s <= sum(bit_depth downto 1);
		c_out <= sum(bit_depth+1);
		
end behavior;