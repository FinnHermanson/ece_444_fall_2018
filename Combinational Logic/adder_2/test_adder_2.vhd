--testbench for 32-bit behavioral adder
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
USE ieee.math_real.ALL;   -- for UNIFORM, TRUNC functions

entity test_adder_2 is
end;

architecture test of test_adder_2 is

component adder_2
	generic(bit_depth 		: integer := 32);
	Port (
		x					: in unsigned(bit_depth-1 downto 0);
		y					: in unsigned(bit_depth-1 downto 0);
		c_in				: in std_logic;
		s					: out unsigned(bit_depth-1 downto 0);
		c_out				: out std_logic
	);
end component;

constant bit_depth			: integer := 32;
constant max_input_value 	: integer := 2**(bit_depth-2);
signal x					: unsigned(bit_depth-1 downto 0) := (others => '0');
signal y					: unsigned(bit_depth-1 downto 0) := (others => '0');
signal c_in					: std_logic := '0';
signal s					: unsigned(bit_depth-1 downto 0) := (others => '0');
signal c_out				: std_logic := '0';

begin

	dev_to_test : adder_2 
		generic map(
			bit_depth => bit_depth)
		port map (
			x => x,
			y => y,
			c_in => c_in,
			s => s,
			c_out => c_out);
	
	stimulus : process	
	-- Variables for testbench
	variable ErrCnt 			: integer := 0 ;
	variable seed1, seed2		: positive; -- Seed values for random generator
	variable rand, rval			: real; -- Random real-number value in range 0 to 1.0

	begin
		for i in 0 to 99999 loop
		
			UNIFORM(seed1, seed2, rand); -- generate random number
			rval := trunc(rand*real(max_input_value));
			x <= to_unsigned(integer(rval),bit_depth);
			
			UNIFORM(seed1, seed2, rand); -- generate random number
			rval := trunc(rand*real(max_input_value));
			y <= to_unsigned(integer(rval),bit_depth);
			
			wait for 10 ns;
			
			if ((c_out & s) /= x+y) then
				report "The adder is broken" severity warning;
				ErrCnt := ErrCnt+1;
			end if;
		end loop;
		
		if (ErrCnt = 0) then 
			report "Success!  Adder Test Completed.";
		else
			report "The adder is broken." severity warning;
		end if;
	end process stimulus;
end test;
	 