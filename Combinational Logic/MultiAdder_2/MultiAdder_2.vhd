library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity MultiAdder_2 is
	generic (bit_depth						: integer :=8);
	Port (
		result								: out signed(bit_depth-1 downto 0);
		A, B, C, D, E, F					: in signed(bit_depth-1 downto 0)
	);
end MultiAdder_2;

architecture behavior of MultiAdder_2 is

	constant num_inputs						: integer := 6;
	
	type input_array is array(0 to num_inputs-1) of signed(bit_depth-1 downto 0);
	signal in_array							:input_array := (others => (others => '0'));
	
	begin
	
	in_array <= (A,B,C,D,E,F);
	
	out_proc : process(in_array)
	variable int_result						: signed(bit_depth-1 downto 0);
	begin
		int_result := (others => '0');
		for i in 0 to num_inputs-1 loop
			int_result := int_result + in_array(i);
		end loop;
		result <= int_result;
	end process out_proc;
end behavior;