library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity ALU is
generic (
		bit_depth					: integer := 32);
port (
		result						: out std_logic_vector(bit_depth-1 downto 0);
		Error						: out std_logic;
		A							: in std_logic_vector(bit_depth-1 downto 0);
		B							: in std_logic_vector(bit_depth-1 downto 0);
		OpCode						: in std_logic_vector(3 downto 0)
	);
end ALU;

architecture behavioral of ALU is
begin
	ALU_proc: process(A, B, OpCode)
	begin
		case OpCode is
			when "0000" => 
				result <= not A;
			when "0001" => 
				result <= not B;
			when "0010" => 
				result <= std_logic_vector(signed(A) + signed(B));
			when "0011" => 
				result <= std_logic_vector(signed(A) - signed(B));
			when "0100" => 
				result <= A(bit_depth-2 downto 0) & '0';
			when "0101" => 
				result <= '0' & '0' & A(bit_depth-1 downto 2);
			when "0110" => 
				result <= B(bit_depth-2 downto 0) & '0';
			when "0111" => 
				result <= '0' & '0' & B(bit_depth-1 downto 2);
			when "1000" => 
				for i in 0 to 31 loop
					result(bit_depth-1-i) <= A(i);
				end loop;
			when "1001" =>
				for i in 0 to 31 loop
					result(bit_depth-1-i) <= B(i);
				end loop;
			when "1010" => 
				result <= A and B;
			when "1011" => 
				result <= A or B;
			when others => 
				result <= (others => 'X');
		end case;
	end process ALU_proc;
end behavioral;

	
