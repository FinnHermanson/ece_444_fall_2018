transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\Homeworks\HW2}
vlib work
vmap work work

# compile the program and test-bench files
vcom ALU.vhd
vcom test_ALU.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_ALU
add wave sim:/test_ALU/dev_to_test/*
 
# define simulation time
run 1600000 ns
# zoom out
wave zoom full