library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

use std.textio.all;
use ieee.std_logic_textio.all;

entity test_ALU is
end;

architecture test of test_ALU is 

component ALU
	generic(bit_depth				: integer := 32);
	Port (
		result						: out std_logic_vector(bit_depth-1 downto 0);
		A							: in std_logic_vector(bit_depth-1 downto 0);
		B							: in std_logic_vector(bit_depth-1 downto 0);
		OpCode						: in std_logic_vector(3 downto 0)
	);
end component;

constant bit_depth					: integer := 32;

signal result						: std_logic_vector(bit_depth-1 downto 0);
signal A_in							: std_logic_vector(bit_depth-1 downto 0) := (others => '0');
signal B_in							: std_logic_vector(bit_depth-1 downto 0) := (others => '0');
signal opcode_in					: std_logic_vector(3 downto 0) := (others => '0');

signal expected_res					: std_logic_vector(bit_depth-1 downto 0);
signal i							: integer;

begin

	dev_to_test : ALU port map (
		A => A_in,
		B => B_in,
		OpCode => opcode_in,
		result => result);
		
	opcode_proc : process(opcode_in, A_in, B_in)
		begin
			case opcode_in is
				when "0000" =>
					expected_res <= not A_in;
				when "0001" =>
					expected_res <= not B_in;
				when "0010" =>
					expected_res <= std_logic_vector(signed(A_in) + signed(B_in));
				when "0011" =>
					expected_res <= std_logic_vector(signed(A_in) - signed(B_in));
				when "0100" =>
					expected_res <= A_in(bit_depth-2 downto 0) & '0';
				when "0101" =>
					expected_res <= '0' & '0' & A_in(bit_depth-1 downto 2);
				when "0110" =>
					expected_res <= B_in(bit_depth-2 downto 0) & '0';
				when "0111" =>
					expected_res <= '0' & '0' & B_in(bit_depth-1 downto 2);
				when "1000" =>
					for i in 0 to 31 loop
						result(bit_depth-1-i) <= A_in(i);
					end loop;
				when "1001" =>
					for i in 0 to 31 loop
						result(bit_depth-1-i) <= B_in(i);
					end loop;
				when "1010" =>
					expected_res <= A_in and B_in;
				when "1011" =>
					expected_res <= A_in or B_in;
				when others =>
					expected_res <= (others => 'X');
			end case;        
	end process opcode_proc; 
	
	stimulus : process
-- Variables for testbench
	variable ErrCnt : integer := 0 ;
	variable WriteBuf : line ;

	begin	
		A_in <= "00000010110011001010000100010010";
		B_in <= "00000010110011001010000100001010";				
	for i in 0 to 15 loop
		opcode_in <= std_logic_vector(to_signed(i, 4));		
		wait for 100 ns;	

		if(expected_res /= result)	then
			write(WriteBuf,	string'("ERROR  ALU test failed at result: opcode = "));	
			write(WriteBuf,	opcode_in);
			writeline(Output, WriteBuf);
			ErrCnt := ErrCnt+1;
		end if;
	end loop; 

	if (ErrCnt = 0) then
		report "SUCCESS!!! ALU Test Completed";
	else
		report "ALU is broken" severity warning; 
	end if;

	end process stimulus;

end test;