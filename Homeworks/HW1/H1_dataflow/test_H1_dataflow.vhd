library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

use std.textio.all ;
use ieee.std_logic_textio.all ;

entity test_H1_dataflow is
end;

architecture test of test_H1_dataflow is

component H1_dataflow
	port (
		F3								: out std_logic;
		F2								: out std_logic;
		F1								: out std_logic;
		F0								: out std_logic;
		A								: in std_logic;
		B								: in std_logic;
		C								: in std_logic;
		D								: in std_logic);
end component;

signal input							: std_logic_vector(3 downto 0);
signal test_output						: std_logic_vector(3 downto 0);
signal exp_output						: std_logic_vector(3 downto 0);

begin
	dev_to_test: H1_dataflow port map (
		F0 => test_output(0), F1 => test_output(1), F2 => test_output(2), F3 => test_output(3),
		A => input(3), B => input(2), C => input(1), D => input(0) 
	);
	
	expected_proc : process(input)
		begin
			case input is
			when "0000" => exp_output <= "1101";		
			when "0001" | "1001" | "1011" => exp_output <= "0111";	
			when "0010" => exp_output <= "1001";	
			when "0011" | "0101" => exp_output <= "1111";		
			when "0100" => exp_output <= "0000";	
			when "0110" | "1100" => exp_output <= "0100";
			when "0111" | "1101" => exp_output <= "1010";
			when "1000" | "1010" | "1110" => exp_output <= "1011";					
			when "1111" => exp_output <= "0101";
			when others => exp_output <= (others => 'X');
			end case;
		end process expected_proc;
		
		stimulus : process
		
		variable ErrCnt : integer := 0;
		variable WriteBuf : line;
		
		begin
			for i in 0 to 15 loop
				input <= std_logic_vector(to_unsigned(i,input'length));
				
				wait for 10 ns;
				if(exp_output /= test_output) then	
					write(WriteBuf, string'("ERROR FA test failed at: input = "));
					write(WriteBuf, input);
					writeline(Output, WriteBuf);
					
					write(WriteBuf, string'("Output = ")); write(WriteBuf, test_output);
					write(WriteBuf, string'(". Exp. Output = ")); write(WriteBuf, exp_output);
					
					writeline(Output, WriteBuf);
					
					ErrCnt := ErrCnt+1;
				end if;
			end loop; -- ieee
			
			if (ErrCnt = 0) then
				report "SUCCESS! H1_dataflow Test Completed.";
			else
				report "The H1_dataflow is broken." severity error;
			end if;
		end process stimulus;
	end test;