library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity H1_dataflow is
	Port (
		F3								: out std_logic;
		F2								: out std_logic;
		F1								: out std_logic;
		F0								: out std_logic;
		A								: in std_logic;
		B								: in std_logic;
		C								: in std_logic;
		D								: in std_logic
	);
end H1_dataflow;

architecture dataflow of H1_dataflow is

begin
	F3 <= ((not B) and (not D)) or 
		  ((not A) and C and D) or 
		  (B and (not C) and D) or 
		  (A and C and (not D));
		  
	F2 <= ((not B) and D) or 
		  ((not A) and (not B) and (not C)) or 
		  ((not A) and (not C) and D) or 
		  (A and C and D) or 
		  ((not A) and B and C and (not D)) or 
		  (A and B and (not C) and (not D));
		  
	F1 <= ((not A) and D) or 
		  ((not C) and D) or 
		  (A and (not B)) or 
		  (A and C and (not D));
		  
	F0 <= (not B) or 
		  (A and C) or 
		  ((not A) and 
		  (not C) and D);
end dataflow;
	 
	