library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity H1_behav is
	Port (
		F3								: out std_logic;
		F2								: out std_logic;
		F1								: out std_logic;
		F0								: out std_logic;
		A								: in std_logic;
		B								: in std_logic;
		C								: in std_logic;
		D								: in std_logic
	);
end H1_behav;

architecture behavior of H1_behav is

signal inputs							: std_logic_vector(3 downto 0);
signal outputs							: std_logic_vector(3 downto 0);

begin
	inputs <= A & B & C & D;
	F3 <= outputs(3);
	F2 <= outputs(2);
	F1 <= outputs(1);
	F0 <= outputs(0);
	
	
	behav_proc : process(inputs)
	begin
		case inputs is
			when "0000" =>
				outputs <= "1101";		
			when "0001" | "1001" | "1011" =>
				outputs <= "0111";	
			when "0010" =>
				outputs <= "1001";	
			when "0011" | "0101" =>
				outputs <= "1111";		
			when "0100" =>
				outputs <= "0000";	
			when "0110" | "1100" =>
				outputs <= "0100";
			when "0111" | "1101" =>
				outputs <= "1010";
			when "1000" | "1010" | "1110" =>
				outputs <= "1011";					
			when "1111" =>
				outputs <= "0101";
			when others =>
				outputs <= (others => 'X');
		end case;
	end process behav_proc;
end behavior;