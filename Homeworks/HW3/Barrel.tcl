transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\Homeworks\HW3}
vlib work
vmap work work

# compile the program and test-bench files
vcom ../sim_mem_init/sim_mem_init.vhd
vcom Barrel.vhd
vcom test_Barrel.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_Barrel
add wave sim:/test_Barrel/dev_to_test/*
 
# define simulation time
run 500 ns
# zoom out
wave zoom full