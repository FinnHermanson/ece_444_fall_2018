library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity Barrel is
generic (
	bit_depth					: integer := 8);
port (
	A							: out std_logic_vector(bit_depth-1 downto 0);
	I							: in std_logic_vector(bit_depth-1 downto 0);
	S							: in std_logic_vector(1 downto 0);
	shift						: in integer range 0 to bit_depth-1;
	reset						: in std_logic;
	clk							: in std_logic);
end Barrel;

architecture behavior of Barrel is

signal A_reg				: std_logic_vector(bit_depth-1 downto 0);
signal Mux_out				: std_logic_vector(bit_depth-1 downto 0);

begin
	-- assign output to registered value
	A <= A_reg;
	-- process for the multiplexor
	Mux_proc: process(I, S, shift, A_reg)
	begin
		case S is
			when "00" => --hold
				Mux_out <= A_reg;
			when "01" => --right shift
				Mux_out <= A_reg(shift-1 downto 0) & A_reg(bit_depth-1 downto shift);
			when "10" => --left shift
				Mux_out <=  A_reg(bit_depth-1-shift downto 0) & A_reg(bit_depth-1 downto bit_depth-shift-1);
			when "11" => --parallel load
				Mux_out <= I;
			when others => -- always include an "error" state!
				Mux_out <= (others => 'X');
		end case;
	end process Mux_proc;
	-- process to create the register
	reg_proc: process(clk)
	begin
		if(rising_edge(clk)) then
			if(reset = '0') then
				A_reg <= (others => '0');
			else
				A_reg <= Mux_out;
			end if;
		end if;
	end process reg_proc;
end behavior;