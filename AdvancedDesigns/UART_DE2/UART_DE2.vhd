library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity UART_DE2 is
port(
	rv_out_seg_1		: out std_logic_vector(6 downto 0);
	rv_out_seg_0		: out std_logic_vector(6 downto 0);
	rv_data_valid		: out std_logic;
	tx_out_seg_1		: out std_logic_vector(6 downto 0);
	tx_out_seg_0		: out std_logic_vector(6 downto 0);
	tx_out				: out std_logic;
	tx_line				: out std_logic;
	rv_in				: in std_logic;
	tx_in				: in std_logic_vector(7 downto 0);
	tx_start			: in std_logic;
	reset				: in std_logic;
	clk					: in std_logic);
end;

architecture behavior of UART_DE2 is

component tUART
generic (
	baud				: integer := 192000);
port (
	data_out			: out std_logic;
	start				: in std_logic;
	data_in				: in std_logic_vector(7 downto 0);
	reset				: in std_logic;
	clk					: in std_logic);
end component;
component rUART
generic (
	baud				: integer := 192000);
port (
	data_out				: out std_logic_vector(7 downto 0);
	data_valid			: out std_logic;
	data_in				: in std_logic;
	reset					: in std_logic;
	clk					: in std_logic);
end component;
component hex_to_7_seg is
port (seven_seg			: out std_logic_vector(6 downto 0);
		hex 			: in std_logic_vector(3 downto 0));
end component;

constant baud			: integer := 75;
signal recv_out			: std_logic_vector(7 downto 0);

begin

	transmit_seg_1: hex_to_7_seg port map(tx_out_seg_1, tx_in(7 downto 4));
	transmit_seg_0: hex_to_7_seg port map(tx_out_seg_0, tx_in(3 downto 0));

	receive_seg_1: hex_to_7_seg port map(rv_out_seg_1, recv_out(7 downto 4));
	receive_seg_0: hex_to_7_seg port map(rv_out_seg_0, recv_out(3 downto 0));

	tx_line <= rv_in;
	
	transmitter: tUART
		generic map(baud)
		port map(tx_out, tx_start, tx_in, reset, clk);
	receiver: rUART
		generic map(baud)
		port map(recv_out, rv_data_valid, rv_in, reset, clk);	
end behavior;