transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\AdvancedDesigns\UART_receiver}
vlib work
vmap work work

# compile the program and test-bench files
vcom ../sim_mem_init.vhd
vcom ../UART_transmitter/tUART.vhd
vcom rUART.vhd
vcom test_rUART.vhd

# initializing the simulation window and adding waves to the simulation window
vsim test_rUART
add wave sim:/test_rUART/recv_under_test/*
add wave sim:/test_rUART/dev_to_test/*
 
# define simulation time
run 5248210 ns
# zoom out
wave zoom full