library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

use std.textio.all ;
use ieee.std_logic_textio.all ;
use work.sim_mem_init.all;

entity test_tUART is
end;

architecture test of test_tUART is
  
component tUART
generic (
	baud : integer 				:= 19200);
port (
	data_out					: out std_logic;
	start						: in std_logic;
	data_in						: in std_logic_vector(7 downto 0);
	reset						: in std_logic;
	clk							: in std_logic);
end component;

constant baud 					: integer := 19200;
signal data_out 				: std_logic;
signal start 					: std_logic := '1';
signal data_in 					: std_logic_vector(7 downto 0) := (others => '0');
signal reset 					: std_logic := '1';
signal clk 						: std_logic := '0';

constant in_fname 				: string := "tUART_input.csv";
file input_file					: text;

begin
	-- testing the UART Transmitter
	dev_to_test:  tUART 
		generic map(baud)
		port map(data_out, start, data_in, reset, clk); 
	
	clk_proc : process
		begin
		wait for 10 ns;
		clk <= not clk;
	end process clk_proc;
	
	stimulus:  process
	variable input_line			: line;
	variable in_char			: character;
	variable in_slv				: std_logic_vector(7 downto 0);
	
	begin
			  
		file_open(input_file, in_fname, read_mode);

		while not(endfile(input_file)) loop
			readline(input_file,input_line);
				
			-- let's read the byte from file
			read(input_line,in_char);
			in_slv := std_logic_vector(to_unsigned(character'pos(in_char),8));
			data_in(7 downto 4) <= ASCII_to_hex(in_slv);
			read(input_line,in_char);
			in_slv := std_logic_vector(to_unsigned(character'pos(in_char),8));
			data_in(3 downto 0) <= ASCII_to_hex(in_slv);
				
			wait for 20 ns;			
			start <= '0';
				
			wait for 20 ns;
			start <= '1';
				
			wait for 600 us;
		end loop;
		
		file_close(input_file);
		report "UART Transmitter Test Completed";							
	end process stimulus;
end test;
