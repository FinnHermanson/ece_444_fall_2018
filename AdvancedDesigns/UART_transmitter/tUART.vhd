library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tUART is
generic (
	baud				: integer := 19200);
port (
	data_out			: out std_logic;
	start				: in std_logic;
	data_in				: in std_logic_vector(7 downto 0);
	reset				: in  std_logic;
	clk					: in std_logic);
end tUART;

architecture behavior of tUART is
constant max_bits				: integer := 10;
constant clk_freq		: integer := 50000000;
constant max_bit_count	: integer := clk_freq / baud;

signal done_shifting	: std_logic;
signal shifting			: std_logic;
signal done_loading		: std_logic;
signal loading			: std_logic;

signal start_proc		: std_logic;
signal start_reg		: std_logic_vector(1 downto 0) := (others => '0');
signal data_reg			: std_logic_vector(9 downto 0) := (others => '0');

signal bit_counter		: integer range 0 to max_bit_count-1;
signal number_bits		: integer range 0 to max_bits-1;

type state_type is(init_state, load_state, shift_state);
signal state, next_state		: state_type;

begin

	data_out <= data_reg(0);

	state_proc : process(clk)
	begin
		if(rising_edge(clk)) then
			if(reset = '0') then
				state <= init_state;
			else
				state <= next_state;
			end if;
		end if;
	end process state_proc;
	
	nxt_state_proc : process(state, start_proc, done_shifting, done_loading)
	begin
		next_state <= state;		loading <= '0';
		shifting <= '0';
		case state is
			when init_state =>
				if start_proc = '1' then
					next_state <= load_state;
				end if;
			when load_state =>
				loading <= '1';
				if (done_loading = '1') then
					next_state <= shift_state;
				end if;
			when shift_state =>
				shifting <= '1';
				if(done_shifting = '1') then
					next_state <= init_state;
				end if;
			when others =>
				next_state <= init_state;
		end case;
	end process nxt_state_proc;

	start_proc <= start_reg(1) and start_reg(0);
	start_reg_proc : process(clk)
	begin
		if(rising_edge(clk)) then
			if(reset = '0') then
				start_reg(1 downto 0) <= (others => '0');
			else
				start_reg(0) <= not start;
				start_reg(1) <= not start_reg(0);
			end if;
		end if;
	end process start_reg_poc;
	
	shift_load_proc : process(clk)
	begin
		if(rising_edge(clk)) then
			if(reset = '0') then
				data_reg <= (others => '0');
			elsif(loading = '1') then
				data_reg <= '1' & data_in & '0';
			elsif(shifting = '1' and bit_counter = (max_bit_count - 1)) then
				data_reg <= '0' & data_reg(9 downto 1);
			end if;
		end if;
	end process shift_load_proc; 
	
	done_loading_proc : process(clk)
	begin
		if(data_reg(9) = '1') then
			done_loading <= '1';
		else
			done_loading <= '0';
		end if;
	end process done_loading_proc;
	
	done_shifting_proc : process(number_bits)
	begin
		if(number_bits = (max_bits-1)) then
			done_shifting <= '1';
		elsif(shifting = '1') then
			done_shifting <= '0';
		end if;
	end process done_shifting_proc;
	
	bit_count_proc	: process(clk)
	begin
		if(rising_edge(clk)) then
			if((reset = '0') or (bit_counter = max_bit_count-1)) then
				bit_counter <= 0;
			elsif(shifting = '1') then
				bit_counter <= bit_counter + 1;
			else
				bit_counter <= 0;
			end if;
		end if;
	end process bit_count_proc;
	
	number_bits_proc : process(clk)
	begin
		if(rising_edge(clk)) then
			if(reset = '0' or (number_bits = max_bits-1)) then
				number_bits <= 0;
			elsif(bit_counter = (max_bit_count - 1)) then
				number_bits <= number_bits + 1;
			end if;
		end if;
	end process number_bits_proc;
	
end behavior;
	