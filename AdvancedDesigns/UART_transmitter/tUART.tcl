transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\AdvancedDesigns\UART_transmitter}
vlib work
vmap work work

# compile the program and test-bench files
vcom ../sim_mem_init.vhd
vcom tUART.vhd
vcom test_tUART.vhd

# initializing the simulation window and adding waves to the simulation window
vsim test_tUART
add wave sim:/test_tUART/dev_to_test/*
 
# define simulation time
run 3000.2 us
# zoom out
wave zoom full