library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.math_real.all;
use IEEE.numeric_std.all;

entity counter_1 is
	Generic (
		max_count					: integer := 2**30;
		synch_reset					: boolean := false);
	Port (
		count 						: out std_logic_vector(integer(ceil(log2(real(max_count))))-1 downto 0);
		strobe						: out std_logic;
		clk							: in std_logic;
		reset						: in std_logic
	);
end counter_1;

architecture behavior of counter_1 is

	constant bit_depth				: integer := integer(ceil(log2(real(max_count))));
	signal count_reg				: unsigned(bit_depth-1 downto 0) := (others => '0');
	
	begin
	count <= std_logic_vector(count_reg);
	
	synch_rst : if synch_reset = true generate
		count_proc: process(clk)
		begin
			if rising_edge(clk) then
				if ((reset = '0') or (count_reg = max_count-1)) then
					count_reg <= (others => '0');
				else
					count_reg <= count_reg + 1;
				end if;
			end if;
		end process;
	end generate;
	asynch_rst : if synch_reset = false generate
		count_proc: process(clk, reset)
			begin
			if(reset = '0') then
				count_reg <= (others => '0');
			elsif rising_edge(clk) then
				if(count_reg = max_count-1) then
					count_reg <= (others => '0');
				else
					count_reg <= count_reg + 1;
				end if;
			end if;
		end process;
	end generate;
	
	output_proc: process(count_reg)
		begin
		strobe <= '0';
		if(count_reg = max_count-1) then
			strobe <= '1';
		end if;
	end process;
end behavior;