transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\SequentialLogic\counter_1}
vlib work
vmap work work

# compile the program and test-bench files
vcom counter_1.vhd
vcom test_counter_1.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_counter_1
add wave sim:/test_counter_1/dev_to_test/*
 
# define simulation time
run 500 ns
# zoom out
wave zoom full