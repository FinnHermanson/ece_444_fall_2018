transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\SequentialLogic\vending_machine}
vlib work
vmap work work

# compile the program and test-bench files
vcom ../sim_mem_init/sim_mem_init.vhd
vcom vending_machine.vhd
vcom test_vending_machine.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_vending_machine
add wave sim:/test_vending_machine/dev_to_test/*
 
# define simulation time
run 1290 ns
# zoom out
wave zoom full