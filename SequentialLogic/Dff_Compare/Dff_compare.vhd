-- Dff_compare

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity Dff_compare is
port (Q					: out std_logic;
	  Q_comb			: out std_logic;
      D					: in std_logic;
      clk				: in std_logic;
      reset				: in std_logic);
end Dff_compare;

architecture behavior of Dff_compare is	

component Dff_1
	port (
		Q				: out std_logic;
		D				: in std_logic;
		clk				: in std_logic;
		reset			: in std_logic);
end component;

component Dff_2
	generic (
		synch_reset		: boolean := true);
	port (
		Q				: out std_logic;
		D				: in std_logic;
		clk				: in std_logic;
		reset			: in std_logic);
end component;
			
constant synch_reset	: boolean := true;

begin
	
	
	comb_dff: Dff_1 port map(
		Q_comb, D, clk, reset);
		
	behav_dff: Dff_2  generic map(synch_reset)
		port map(Q, D, clk, reset);
		
end behavior;