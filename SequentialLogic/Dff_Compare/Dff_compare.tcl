transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\SequentialLogic\Dff_Compare}
vlib work
vmap work work

# compile the program and test-bench files
vcom ../Dff1/Dff_1.vhd
vcom ../Dff2/Dff_2.vhd
vcom Dff_compare.vhd
vcom test_Dff_compare.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_Dff_compare
add wave sim:/test_Dff_compare/dev_to_test/*
 
# define simulation time
run 300 ns
# zoom out
wave zoom full