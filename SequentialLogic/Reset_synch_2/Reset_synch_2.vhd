library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity reset_synch_2 is
	Port (
		sys_reset		: out std_logic;
		reset_in		: in std_logic;
		clk				: in std_logic
	);
end reset_synch_2;

architecture behavior of reset_synch_2 is

	signal q_synch		: std_logic;
	
	begin 
	
	sys_reset <= q_synch;
	
	reset_proc: process(clk, reset_in)
		begin
		if rising_edge(clk) then
			q_synch <= reset_in;
		end if;
	end process;

end behavior;