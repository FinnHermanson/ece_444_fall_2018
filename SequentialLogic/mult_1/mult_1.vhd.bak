library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

entity mult_1 is
generic (
	input_size			: integer := 8);
port (
	product				: out unsigned(2*input_size-1 downto 0);
	data_ready			: out std_logic;
	input_1				: in unsigned(input_size-1 downto 0);
	input_2				: in unsigned(input_size-1 downto 0);
	start				: in std_logic;
	reset				: in std_logic;
	clk					: in std_logic);
end mult_1;

architecture behavior of mult_1 is

type state_type is(init, load, right_shift, done);
signal state, nxt_state		: state_type;

signal shift			: std_logic;
signal add_and_shift	: std_logic;
signal load_data		: std_logic;

constant maxcount		: integer := input_size-1;
signal input_1_reg		: unsigned(2*input_size-1 downto 0) := (others => '0');
signal sum				: unsigned(2*input_size-1 downto 0) := (others => '0');
signal product_reg		: unsigned(2*input_size-1 downto 0) := (others => '0');
signal count			: integer range 0 to maxcount := 0;
signal start_mult_lead	: std_logic := '0';
signal start_mult_follow: std_logic := '0';
signal start_mult		: std_logic := '0';

begin
	start_mult <= start_mult_lead and (not start_mult_follow);
	start_mult_proc: process(clk)
		begin
		if(rising_edge(clk)) then
			if(reset = '0') then	
				start_mult_lead <= '0';
				start_mult_follow <= '0';
			else
				start_mult_lead <= start;
				start_mult_follow <start_mult_lead;
			end if;
		end if;
	end process;
	
	state_proc: process(clk)
		begin
		if (rising_edge(clk)) then
			if(reset = '0') then
				state <= init;
			else
				state <= nxt_state;
			end if;
		end if;
	end process state_proc;
	state_machine: process(state, start, start_mult, count, product_reg(0))
		begin
		nxt_state <= state;
		shift <= '0';
		add_and_shift <= '0';
		load_data <= '0';
		data_ready <= '0';
		
		case state is
			when init =>
				if(start_mult = '1') then
					nxt_state <= load;
				end if;
			when load =>
				load_data <= '1';
				nxt_state <= right_shift;
			when right_shift =>
				if(count = maxcount) then
					nxt_state <= done;
				end if;
				if(product_reg(0) = '1') then
					add_and_shift <= '1';
				else
					shift <= '1';
				end if;
			when done =>
				data_ready <= '1';
				if(start = '0') then
					nxt_state <= init;
				end if;
			when others =>
				nxt_state <= init;
			end case;
	end process state_machine;
	
	count_proc: process(clk)
		begin
		if(rising_edge(clk)) then
			if((start_mult ='1') or (reset ='0') or (count = maxcount)) then
				count <= 0;
			elsif(state = right_shift) then
				count <= count + 1;
			end if;
		end if;
	end process count_proc;
	
	sum <= product_reg + input_1_reg;
	
	mult_proc:process(clk)
		begin
		if(rising_edge(clk)) then
			if(reset = '0') then
				product_reg <= ?;
				input_1_reg <= ?;
			elsif(load_data = '1') then
				product_reg <= ?;
				input_1_reg <= ?;
			elsif(add_and_shift = '1') then
				product_reg <= ?;
			elsif(shift = '1') then
				product_reg <= ?;
			end if;
		end if;
	end process mult_proc;
	
	product <= product_reg;
end behavior;
	