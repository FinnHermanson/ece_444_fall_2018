library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity memory_1 is 
generic (
	addr_size			: integer := 256;
	data_width			: integer := 8
	);
port (
	data_out			: out std_logic_vector(data_width-1 downto 0);
	data_in				: in std_logic_vector(data_width-1 downto 0);
	read_addr			: in std_logic_vector(integer(ceil(log2(real(addr_size))))-1 downto 0);
	write_addr			: in std_logic_vector(integer(ceil(log2(real(addr_size))))-1 downto 0);
	clk					: in std_logic;
	write_en			: in std_logic);
end memory_1;

architecture behavior of memory_1 is

type MemType is array (0 to addr_size-1) of std_logic_vector(data_width-1 downto 0);
signal Mem				: MemType;

begin

	mem_write: process(clk)
	begin
		if(rising_edge(clk)) then
			if(write_en = '1') then
				Mem(to_integer(unsigned(write_addr))) <= data_in;
				end if;
		end if;
	end process mem_write;
	
	mem_read: process(clk)
		begin
		if(rising_edge(clk)) then
			data_out <= Mem(to_integer(unsigned(read_addr)));
		end if;
	end process mem_read;
end behavior;

