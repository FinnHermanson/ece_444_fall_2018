library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.math_real.all;
use IEEE.numeric_std.all;

use std.textio.all ;
use ieee.std_logic_textio.all ;
use work.sim_mem_init.all;

entity test_memory_1 is
end;
architecture test of test_memory_1 is
  
component memory_1
generic (
	addr_size 			: integer := 256;
	data_width 			: integer := 8);
port (
	data_out			: out std_logic_vector(data_width-1 downto 0);
    data_in				: in std_logic_vector(data_width-1 downto 0);
    read_addr			: in std_logic_vector(integer(ceil(log2(real(addr_size))))-1 downto 0);
    write_addr			: in std_logic_vector(integer(ceil(log2(real(addr_size))))-1 downto 0);
    clk					: in std_logic;
	write_en			: in std_logic);
end component;

constant addr_size 		: integer := 256;
constant data_width 	: integer := 8;
constant in_fname 		: string := "mem1_input.csv";
constant out_fname 		: string := "mem1_output.csv";
file input_file			: text;
file output_file		: text;

signal data_in 			: std_logic_vector(data_width-1 downto 0) := x"00";
signal data_out 		: std_logic_vector(data_width-1 downto 0);
signal addr 			: std_logic_vector(integer(ceil(log2(real(addr_size))))-1 downto 0);
signal expected_data 	: std_logic_vector(data_width-1 downto 0);

signal clk 				: std_logic;
signal writen 			: std_logic;

begin
	-- testing the memory
	dev_to_test:  memory_1 
		generic map(addr_size, data_width)
		-- equate the read and write addresses for simplicity
		port map(data_out, data_in, addr, addr, clk, writen); 
	
	stimulus:  process
	
	variable input_line	: line;
	variable WriteBuf 	: line ;
	variable in_char	: character;
	variable in_slv		: std_logic_vector(7 downto 0);
	variable out_slv	: std_logic_vector(7 downto 0);

	-- Variables for testbench
	variable ErrCnt 	: integer := 0 ;
	variable line_num 	: integer := 0;
		
	begin
		
		file_open(input_file, in_fname, read_mode);
		file_open(output_file, out_fname, read_mode);
		
		while not(endfile(input_file)) loop
                
			readline(input_file,input_line);
			
			-- let's read the first 10 characters in the row
			for i in 0 to 9 loop
				read(input_line,in_char);
				in_slv := std_logic_vector(to_unsigned(character'pos(in_char),8));
					  
				-- i is the column number of the input file
				if( i = 3 ) then
					data_in(7 downto 4) <= ASCII_to_hex(in_slv);
				elsif( i = 4 ) then
					data_in(3 downto 0) <= ASCII_to_hex(in_slv);
				elsif( i = 6 ) then
					addr(7 downto 4) <= ASCII_to_hex(in_slv);
				elsif( i = 7 ) then
					addr(3 downto 0) <= ASCII_to_hex(in_slv);
				elsif( i = 9 ) then
					writen <= in_slv(0);
				end if;
			end loop;
				  
			readline(output_file,input_line);			
			clk <= '0';
			wait for 10 ns;
		  
			for i in 0 to 4 loop
				read(input_line,in_char);
				out_slv := std_logic_vector(to_unsigned(character'pos(in_char),8));
					  
				-- i is the column number of the output file
				if( i =3 ) then
					expected_data(7 downto 4) <= ASCII_to_hex(out_slv);
				elsif( i=4 ) then
					expected_data(3 downto 0) <= ASCII_to_hex(out_slv);
				end if;
			end loop;
		
			clk <= '1';
			wait for 10 ns;
		
			if ((expected_data /= data_out) and writen = '0' and 
				data_out /= "UUUUUUUU") then
				write(WriteBuf, string'("ERROR:  memory failed at line = "));
				write(WriteBuf, line_num);
				write(WriteBuf, string'("expected = "));
				write(WriteBuf, expected_data);
				write(WriteBuf, string'(", data_out = "));
				write(WriteBuf, data_out);
		
				writeline(Output, WriteBuf);
				ErrCnt := ErrCnt+1;
			end if; 
				  
			line_num := line_num + 1;
		end loop;
		
		file_close(input_file);
		file_close(output_file);
  
		if (ErrCnt = 0) then 
			report "SUCCESS!!!  memory_1 Test Completed";
		else
			report "memory_1 device is broken" severity warning;
		end if;
	end process stimulus;
end test;