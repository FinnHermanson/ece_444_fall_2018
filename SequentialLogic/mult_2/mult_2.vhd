library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity mult_2 is
generic (
	input_size						: integer := 8);
port (
	product							: out unsigned(2*input_size-1 downto 0);
	data_ready						: out std_logic;
	input_1							: in unsigned(input_size-1 downto 0);
	input_2							: in unsigned(input_size-1 downto 0);
	reset							: in std_logic;
	start							: in std_logic;
	clk								: in std_logic);
end mult_2;

architecture behavior of mult_2 is

signal input_1_reg					: unsigned(input_size-1 downto 0);
signal input_2_reg					: unsigned(input_size-1 downto 0);
signal data_ready_reg				: std_logic;

begin
	data_ready <= data_ready_reg;
	
	input_proc: process(clk)
	begin
		if(rising_edge(clk)) then
			if(reset = '0') then
				input_1_reg <= (others => '0');
				input_2_reg <= (others => '0');
				data_ready_reg <= '0';
			elsif(start = '1') then
				input_1_reg <= input_1;
				input_2_reg <= input_2;
				data_ready_reg <= '0';
			else
				data_ready_reg <= '1';
			end if;
		end if;
	end process input_proc;
	product <= input_1_reg * input_2_reg;
end behavior;