transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\SequentialLogic\mult_2}
vlib work
vmap work work

# compile the program and test-bench files
vcom ../sim_mem_init/sim_mem_init.vhd
vcom mult_2.vhd
vcom test_mult_2.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_mult_2
add wave sim:/test_mult_2/dev_to_test/*
 
# define simulation time
run 970 ns
# zoom out
wave zoom full