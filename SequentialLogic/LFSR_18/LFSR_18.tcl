transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {D:\Documents\School\Fourth_Year\ECE_444_501\VHDL\Sequential_Logic\LFSR_18}
vlib work
vmap work work

# compile the program and test-bench files
vcom LFSR_18.vhd
vcom test_LFSR_18.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_LFSR_18
add wave sim:/test_LFSR_18/dev_to_test/*
 
# define simulation time
run 5242890 ns
# zoom out
wave zoom full