transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\SequentialLogic\debounce}
vlib work
vmap work work

# compile the program and test-bench files
vcom debounce.vhd
vcom test_debounce.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_debounce
add wave sim:/test_debounce/dev_to_test/*
 
# define simulation time
run 5050 ns
# zoom out
wave zoom full