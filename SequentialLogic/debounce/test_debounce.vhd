library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

use std.textio.all ;
use ieee.std_logic_textio.all ;

entity test_debounce is
end;

architecture test of test_debounce is

component debounce is
	generic (
		maxcount 		: integer := 500000
	);
	Port (
		switch_db		: out std_logic;
		switch_in     	: in std_logic;
		reset			: in std_logic;
		clk				: in std_logic
	);
end component;

signal sw_out		: std_logic := '0';
signal sw_in        : std_logic := '0';
signal reset_in		: std_logic := '0';
signal clk_in       : std_logic := '0';
constant maxcount 	: integer := 10;

begin

dev_to_test : debounce 
generic map (
	maxcount )
port map (
	switch_db => sw_out,
	switch_in => sw_in,
	clk => clk_in,
	reset => reset_in);
	
clk_proc : process
	begin
    wait for 10 ns;
    clk_in <= not clk_in;
end process clk_proc;

reset_proc : process
  begin
      wait for 15 ns;
      reset_in <= '1';
end process reset_proc;

sw_proc : process
	begin
	wait for 500 ns;
	sw_in <= not sw_in;
	wait for 500 ns;
	sw_in <= not sw_in;
	wait for 500 ns;
	sw_in <= not sw_in;
	wait for 50 ns;
	sw_in <= not sw_in;
	wait for 500 ns;
	sw_in <= not sw_in;
end process sw_proc;
	
end test;
	 

