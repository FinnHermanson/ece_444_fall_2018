transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {C:\_Eric\UD\Teaching\ECE501\Designs\Sequential_Logic\shift_reg}
vlib work
vmap work work

# compile the program and test-bench files
vcom shift_reg.vhd
vcom test_shift_reg.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_shift_reg
add wave sim:/test_shift_reg/dev_to_test/*
 
# define simulation time
run 350 ns
# zoom out
wave zoom full
