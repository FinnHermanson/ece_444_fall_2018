library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity P_load_2 is
generic (
	data_width			: integer := 8
	);
port (data_out			: out std_logic_vector(data_width-1 downto 0);
	  data_in			: in std_logic_vector(data_width-1 downto 0);
	  load				: in std_logic;
	  reset				: in std_logic;
	  clk				: in std_logic);
end P_load_2;

architecture behavior of P_load_2 is

signal data_reg			: std_logic_vector(data_width-1 downto 0);

begin

	data_out <= data_reg;
	
	P_load_proc: process(clk, reset)
	begin	
		if(reset = '0') then
			data_reg <= (others => '0');
		elsif(rising_edge(clk)) then
			if(load = '1') then	
				data_reg <= data_in;
--			else
--				data_reg <= data_reg;
			end if;
		end if;
	end process P_load_proc;
end behavior;