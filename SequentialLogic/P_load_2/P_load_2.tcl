transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {D:\Documents\School\Fourth_Year\ECE_444_501\VHDL\Sequential_Logic\P_load_2}
vlib work
vmap work work

# compile the program and test-bench files
vcom ../sim_mem_init/sim_mem_init.vhd
vcom P_load_2.vhd
vcom test_P_load_2.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_P_load_2
add wave sim:/test_P_load_2/dev_to_test/*
 
# define simulation time
run 420 ns
# zoom out
wave zoom full
