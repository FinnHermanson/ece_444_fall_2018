library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

use std.textio.all ;
use ieee.std_logic_textio.all ;

entity test_reset_synch_1 is
end;

architecture test of test_reset_synch_1 is

component reset_synch_1 is
	Port (
		-- main board i/o
		sys_reset 		: out std_logic;
		reset_in	 	: in std_logic;
		clk     		: in std_logic
	);
end component;

signal sys_reset_out		: std_logic;
signal reset_in		    	: std_logic := '0';
signal clk_in           	: std_logic := '0';

begin

dev_to_test : reset_synch_1 port map (
	sys_reset => sys_reset_out,
	clk => clk_in,
	reset_in => reset_in);
	
clk_proc : process
  begin
      wait for 10 ns;
      clk_in <= not clk_in;
end process clk_proc;

reset_proc : process
  begin
      wait for 25 ns;
      reset_in <= '1';
	  wait for 49 ns;
	  reset_in <= '0';
	  wait for 26 ns;
	  reset_in <= '1';
	  
end process reset_proc;
	
end test;
	 

