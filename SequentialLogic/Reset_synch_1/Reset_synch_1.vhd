library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity reset_synch_1 is
	Port (
		sys_reset		: out std_logic;
		reset_in		: in std_logic;
		clk				: in std_logic
	);
end reset_synch_1;

architecture behavior of reset_synch_1 is

	signal q_lead, q_follow		: std_logic;
	
	begin 
	
	sys_reset <= q_follow;
	
	reset_proc: process(clk, reset_in)
		begin
		if(reset_in = '0') then
			q_lead <= '0';
			q_follow <= '0';
		elsif rising_edge(clk) then
			q_lead <= '1';
			q_follow <= q_lead;
		end if;
	end process;

end behavior;