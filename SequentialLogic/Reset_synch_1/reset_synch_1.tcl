transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {D:\Documents\School\Fourth_Year\ECE_444_501\VHDL\Sequential_Logic\Reset_synch_1}
vlib work
vmap work work

# compile the program and test-bench files
vcom reset_synch_1.vhd
vcom test_reset_synch_1.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_reset_synch_1
add wave sim:/test_reset_synch_1/dev_to_test/*
 
# define simulation time
run 400 ns
wave zoom full
