transcript off
#stop previous simulations
quit -sim	

# select a directory for creation of the work directory
cd {\\Mac\Home\Documents\School\4)SeniorYear\ECE444\SequentialLogic\Dff2}
vlib work
vmap work work

# compile the program and test-bench files
vcom Dff_2.vhd
vcom test_Dff_2.vhd 

# initializing the simulation window and adding waves to the simulation window
vsim test_Dff_2
add wave sim:/test_Dff_2/dev_to_test/*
 
# define simulation time
run 166 ns
# zoom out
wave zoom full